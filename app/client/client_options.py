import os, json


class ClientOptions:
  def __init__(self, kwargs: dict = None):
    self.width = kwargs.get('width', 800)
    self.height = kwargs.get('height', 600)
    self.background = kwargs.get('background', 'bg01')
    self.fullscreen = kwargs.get('fullscreen', False)
    self.double_buffer = kwargs.get('double_buffer', True)
    self.play_music = kwargs.get('play_music', True)
    self.music_volume = kwargs.get('music_volume', 0.3)

    self.static_dir = os.environ.get('CLIENT_STATIC_DIR', './static')

  def dump(self):
    return {
      'width': self.width,
      'height': self.height,
      'background': self.background,
      'fullscreen': self.fullscreen,
      'double_buffer': self.double_buffer,
      'play_music': self.play_music,
      'music_volume': self.music_volume
    }

  @staticmethod
  def load(filepath = ''):
    data = {}
    save_options = True

    if os.path.isfile(filepath):
      with open(filepath, 'r') as fp:
        data = json.load(fp)
        save_options = False

    result = ClientOptions(kwargs=data)

    if save_options:
      with open(filepath, 'w') as fp:
        json.dump(result.dump(), fp, indent=4, sort_keys=True)

    return result