import os
os.environ['PYGAME_HIDE_SUPPORT_PROMPT'] = '1'

PLAY_MUSIC_END_EVENT = 19237855

from .pkg import version

from .client_options import ClientOptions
from .client import Client