import os

from app.client import Client, ClientOptions

CLIENT_DATA_DIR = os.environ.get('CLIENT_DATA_DIR', './data')

options = ClientOptions.load(f'{CLIENT_DATA_DIR}/client_options.json')
client = Client(options=options)

client.run()