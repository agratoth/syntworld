import pygame as pg
import pygame_gui as pgg


class StartScreen:
  def __init__(self, client=None):
    pg.font.init()

    self._client = client

    self.render_gui()

  def update(self, timedelta):
    self.gui.update(timedelta)

  def process_event(self, event):
    if event.type == pg.USEREVENT:
      if event.user_type == pgg.UI_BUTTON_PRESSED:
        if event.ui_element.id == 'exit_button':
          self._client.is_closed = True
    self.gui.process_events(event)

  def render_info(self, screen):
    font = pg.font.Font(f'{self._client.options.static_dir}/fonts/titillium.ttf', 16)

    screen.blit(
      font.render(f'Client version: {self._client.version}', True, (255, 255, 255)),
      (16, self._client.height - 32))

  def render_gui(self):
    self.gui = pgg.UIManager(self._client.size, f'{self._client.options.static_dir}/gui/theme.json')
    
    buttons = [
      {'id':'new_sim_button', 'text': 'New simulation'},
      {'id':'connect_button', 'text': 'Connect to simulation'},
      {'id':'options_button', 'text': 'Options'},
      {'id':'creds_button', 'text': 'Credentials'},
      {'id':'exit_button', 'text': 'Exit'},
    ]

    top_offset = self._client.height - len(buttons)*50 - 50
    left_offset = self._client.width / 2 - 110

    for idx, button in enumerate(buttons):
      bt = pgg.elements.UIButton(
        relative_rect=pg.Rect((left_offset, top_offset + (40+10)*idx), (220, 40)), 
        text=button.get('text'), 
        manager=self.gui)
      bt.id = button.get('id')

  def draw(self):
    screen = pg.Surface(self._client.size, pg.SRCALPHA)
    background = self._client.get_static_image(f'backgrounds/{self._client.options.background}/image.jpg')
    screen.blit(background, (0,0))

    letters = [
      self._client.get_static_image('images/letter_s.png'),
      self._client.get_static_image('images/letter_y.png'),
      self._client.get_static_image('images/letter_n.png'),
      self._client.get_static_image('images/letter_t.png'),
    ]

    letter_aspect = letters[0].get_width() / letters[0].get_height()
    letter_height = self._client.height / 4
    letter_width = letter_height * letter_aspect

    left_offset = ((self._client.width - 160) - (4 * letter_width))/2

    self.gui.draw_ui(screen)

    for idx, letter in enumerate(letters):
      size = (int(letter_width), int(letter_height))

      letter = pg.transform.smoothscale(letter, size)
      screen.blit(letter, (left_offset + (letter_width * idx) + ((idx+1)*32),100))

    self.render_info(screen)

    return screen