import os, random

import pygame as pg

from . import ClientOptions, screens, version, PLAY_MUSIC_END_EVENT


class Client:
  def __init__(self, options: ClientOptions = None):
    self.options = options
    self._width = self.options.width
    self._height = self.options.height
    self.version = version

    self.is_closed = False
    self._current_screen = 'start_screen'

    self._screens = {
      'start_screen': screens.StartScreen(client=self)
    }

    flags = pg.HWSURFACE
    if self.options.double_buffer: flags |= pg.DOUBLEBUF
    if self.options.fullscreen: flags |= pg.FULLSCREEN

    self.screen = pg.display.set_mode((self.options.width, self.options.height), flags)
    

  @property
  def width(self):
    return self._width

  @property
  def height(self):
    return self._height

  @property
  def size(self):
    return (self._width, self._height)

  def play_ambient(self):
    if self.options.play_music:
      sounds = os.listdir(f'{self.options.static_dir}/sound/ambient')

      volume = max(min(self.options.music_volume, 1.0), 0)

      sound = f'{self.options.static_dir}/sound/ambient/{random.choice(sounds)}'
      pg.mixer.music.load(sound)
      pg.mixer.music.set_volume(self.options.music_volume)
      pg.mixer.music.play()

  def get_static_image(self, path):
    return pg.image.load(f'{self.options.static_dir}/{path}')

  def reload_screen(self):
    self.screen.blit(self._screens[self._current_screen].draw(), (0,0))
    pg.display.update()

  def run(self):
    pg.mixer.init()
    pg.mixer.music.set_endevent(PLAY_MUSIC_END_EVENT)

    timedeltas = []

    clock = pg.time.Clock()

    pg.display.set_caption(f'Syntworld {self.version}')
    pg.display.set_icon(self.get_static_image('images/icon.png'))

    self.play_ambient()

    while not self.is_closed:
      timedelta = clock.tick(24) / 1000.0
      timedeltas.append(timedelta)
      if len(timedeltas) > 25:
        timedeltas.pop(0)

      for event in pg.event.get():
        if event.type == pg.QUIT:
          self.is_closed = True
        elif event.type == pg.VIDEORESIZE or event.type == pg.VIDEOEXPOSE:
          self.screen.fill((0,0,0))
          self._width = self.screen.get_width()
          self._height = self.screen.get_height()
          self._screens[self._current_screen].render_gui()
          self.reload_screen()
        elif event.type == PLAY_MUSIC_END_EVENT:
          self.play_ambient()

        self._screens[self._current_screen].process_event(event)

      self._screens[self._current_screen].update(timedelta)

      self.reload_screen()
